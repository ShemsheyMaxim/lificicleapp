package com.maxim.lifecicleapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LifeCycleActivity1 extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "myLogs";
    AlertDialog.Builder ad;
    Context context;
    Button btnAlertDialog,buttonNextActivity;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle1);
        Log.d(TAG, "Life cycle onCreate");

        editText = (EditText) findViewById(R.id.editText);
        btnAlertDialog = (Button) findViewById(R.id.button);
        buttonNextActivity = (Button) findViewById(R.id.button_next_activity);

        btnAlertDialog.setOnClickListener(this);
        buttonNextActivity.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Life cycle onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Life cycle onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Life cycle onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Life cycle onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "Life cycle onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Life cycle onDestroy");
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                context = LifeCycleActivity1.this;
                String title = "Выбор есть всегда";
                String message = "Это портретный режим?";
                String button1String = "Да";
                String button2String = "Нет";

                ad = new AlertDialog.Builder(context);
                ad.setTitle(title);  // заголовок
                ad.setMessage(message); // сообщение
                ad.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        Toast.makeText(context, "Вы сделали правильный выбор",
                                Toast.LENGTH_LONG).show();
                    }
                });
                ad.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        Toast.makeText(context, "Возможно вы правы", Toast.LENGTH_LONG)
                                .show();
                    }
                });
                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        Toast.makeText(context, "Вы ничего не выбрали",
                                Toast.LENGTH_LONG).show();
                    }
                });
                ad.show();
                Log.d(TAG, "Enter mode alert dialog");
                break;
            case R.id.button_next_activity:
                Intent myIntent = new Intent(LifeCycleActivity1.this, LifeCycleActivity2.class);
                myIntent.putExtra("editText", editText.getText().toString()); //Optional parameters
                LifeCycleActivity1.this.startActivity(myIntent);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.putString(EDIT_TEXT_KEY, editText.getText());

        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }
}
