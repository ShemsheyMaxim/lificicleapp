package com.maxim.lifecicleapp;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class LifeCycleActivity2 extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "myLogs";
    EditText editTextFromActivity2;
    TextView textView;
    BlankFragment1ForActivity2 fragment1;
    BlankFragment2ForActivity2 fragment2;
    FragmentTransaction fragmentTransaction;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle2);
        Log.d(TAG, "Life cycle 2 onCreate");

        Intent intent = getIntent();
        String value = intent.getStringExtra("editText");

        textView = (TextView) findViewById(R.id.textView);
        textView.setText(value);

        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Life cycle 2 onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Life cycle 2 onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Life cycle 2 onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Life cycle 2 onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "Life cycle 2 onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Life cycle 2 onDestroy");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.putString(EDIT_TEXT_KEY, editText.getText());

        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState2");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    public void onClick(View view) {
        fragment1 = new BlankFragment1ForActivity2();
        fragment2 = new BlankFragment2ForActivity2();
        fragmentTransaction = getFragmentManager().beginTransaction();
        switch (view.getId()) {
            case (R.id.button_select_fragment_1):
                fragmentTransaction.add(R.id.frame_layout, fragment1);
                Log.d(TAG, "add fragment1");
                break;
            case (R.id.button_select_fragment_2):
                fragmentTransaction.replace(R.id.frame_layout, fragment2);
                Log.d(TAG, "replace fragment 1 to fragment 2");
                break;
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
